<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;
use KBNT_Framework\Helpers\General;
use KBNT_Framework\Models\BlockCategory;

class Gutenberg extends AbstractComponent
{

	private $lazyload_youtube_embed = false;
	private $fix_quotes_in_code = false;

	private $support_custom_colors = false;
	private $colors;
	private $allow_custom_colors;
	private $gradients;
	private $allow_custom_gradients;
	private $disable_editor_fullscreen_mode = true;
	private $disable_directory_search = true;
	private $reusable_blocks_in_menu = false;
	private $disable_color_settings = false;
	private $disable_empty_paragraph_blocks = false;

	private $block_categories = [];

	/**
	 * Setter
	 */
	public function lazyload_youtube()
	{
		$this->lazyload_youtube_embed = true;
	}

	public function fix_quotes_in_code()
	{
		$this->fix_quotes_in_code = true;
	}

	public function enable_auto_fullscreen_mode()
	{
		$this->disable_editor_fullscreen_mode = false;
	}

	public function enable_directory_search()
	{
		$this->disable_directory_search = false;
	}

	public function add_reusable_blocks_to_menu()
	{
		$this->reusable_blocks_in_menu = true;
	}

	public function disable_gutenberg_color_settings_tab()
	{
		$this->disable_color_settings = true;
	}

	/**
	 * Add support for custom colors
	 *
	 * @source Náš WP package
	 * @param mixed $colors
	 * @param bool $allow_custom_colors
	 * @param mixed|null $gradients
	 * @param bool $allow_custom_gradients
	 * @return void
	 */
	public function colors_support($colors, $allow_custom_colors = true, $gradients = null, $allow_custom_gradients = true)
	{
		$this->support_custom_colors = true;
		$this->colors = $colors;
		$this->allow_custom_colors = $allow_custom_colors;
		$this->gradients = $gradients;
		$this->allow_custom_gradients = $allow_custom_gradients;
	}

	/**
	 * Add new block category to existing
	 * @param BlockCategory $category
	 * @return void
	 */
	public function add_block_category($category)
	{
		$this->block_categories[] = $category->get();
	}

	/**
	 * Don't empty paragraphs from Gutenberg content
	 * @return void
	 */
	public function dont_render_empty_paragraphs() {
		$this->disable_empty_paragraph_blocks = true;
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{
		if ($this->lazyload_youtube_embed) {
			add_filter('render_block', [$this, 'wp_lazyload_youtube'], 999, 2);
		}
		if ($this->fix_quotes_in_code) {
			add_filter('render_block', [$this, 'wp_fix_quotes'], 999, 2);
		}

		if ($this->disable_color_settings) {
			// @see https://wpdevelopment.courses/articles/gutenberg-colour-settings/
			add_theme_support('disable-custom-colors');
			add_theme_support('disable-custom-colors');
			add_theme_support('editor-color-palette');
			add_theme_support('editor-gradient-presets', []);
			add_theme_support('disable-custom-gradients');
		} elseif ($this->support_custom_colors) {
			$editor_colors = array();
			foreach ($this->colors as $key => $value) {
				$editor_colors[] = array(
					'name' => __($key, 'naswp-kit-atomic'),
					'slug' => sanitize_title($key),
					'color' => $value,
				);
			}

			add_theme_support('editor-color-palette', $editor_colors);

			if (!$this->allow_custom_colors) {
				add_theme_support('disable-custom-colors');
			}

			$editor_gradients = array();
			if ($this->gradients) {
				foreach ($this->gradients as $key => $value) {
					$editor_gradients[] = array(
						'name' => __($key, 'naswp-kit-atomic'),
						'slug' => sanitize_title($key),
						'gradient' => $value,
					);
				}

				add_theme_support('editor-gradient-presets', $editor_gradients);
			}

			if (!$this->allow_custom_gradients) {
				add_theme_support('disable-gradient-presets');
			}
		}

		if ($this->disable_editor_fullscreen_mode && is_admin()) {
			/**
			 * Source: https://www.alektra.net/how-to-automatically-disable-fullscreen-mode-in-wordpress/
			 */
			add_action('enqueue_block_editor_assets', function () {
				wp_add_inline_script('wp-blocks', "jQuery( window ).load(function() { const isFullscreenMode = wp.data.select( 'core/edit-post' ).isFeatureActive( 'fullscreenMode' ); if ( isFullscreenMode ) { wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fullscreenMode' ); } });");
			});
		}

		if ($this->disable_directory_search) {
			/**
			 * Source: https://github.com/WordPress/gutenberg/issues/23961.
			 */
			add_action(
				'after_setup_theme',
				function () {
					\remove_action('enqueue_block_editor_assets', 'wp_enqueue_editor_block_directory_assets');
					\remove_action('enqueue_block_editor_assets', 'gutenberg_enqueue_block_editor_assets_block_directory');
				}
			);
		}

		if ($this->reusable_blocks_in_menu) {
			/**
			 * Reusable Blocks accessible in backend
			 * @link https://www.billerickson.net/reusable-blocks-accessible-in-wordpress-admin-area
			 *
			 */
			add_action('admin_menu', function () {
				add_menu_page('Reusable Blocks', 'Reusable Blocks', 'edit_posts', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22);
			});
		}

		if ($this->disable_empty_paragraph_blocks) {
			add_filter('render_block_core/paragraph', function ($block_content, $block)
				{
					$cleaned_content = preg_replace('/\s+/', '', $block_content);
					if (trim($cleaned_content) !== '<p></p>') {
						return $block_content;
					}
				}, 10, 2);
		}

		if ($this->block_categories) {
			/**
			 * Add block category
			 * @param array $categories
			 * @param WP_Post $post
			 * @return array
			 */
			add_filter('block_categories', function ($categories, $post) {
				return \array_merge($categories, $this->block_categories);
			}, 10, 2);
		}
	}

	/**
	 * Native lazyload for youtube block
	 *
	 * @source https://css-tricks.com/lazy-load-embedded-youtube-videos/
	 *
	 * @param string $block_content The block content about to be appended.
	 * @param array $block The full block, including name and attributes.
	 * @return string
	 */
	public function wp_lazyload_youtube($block_content, $block)
	{
		if ($block['blockName'] === 'core-embed/youtube') {

			$ytb_id = General::get_youtube_id($block['attrs']['url']);

			if ($ytb_id) {

				// LATER - get better video alt
				$youtube = "<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;z-index:1;margin:auto}span{height:48px;text-align:center;font:25px/48px sans-serif;color:white}span::before{content:'';width:68px;height:48px;position:absolute;top:50%;left:50%;z-index:-1;transform:translate(-50%,-50%);background:#212121;opacity:.8;border-radius:8px;transition:background-color .1s cubic-bezier(0.4,0.0,1,1),opacity .1s cubic-bezier(0.4,0.0,1,1)}a:hover span::before{background-color:red;opacity:1}</style><a href='https://www.youtube.com/embed/$ytb_id?autoplay=1'><img src='https://img.youtube.com/vi/$ytb_id/hqdefault.jpg' alt='YouTube video'><span>▶</span></a>";
				$youtube = \str_replace(['<', '>'], ['&lt;', '&gt;'], $youtube);

				$block_content = str_replace('allowfullscreen', 'loading="lazy" srcdoc="' . $youtube . '" allowfullscreen', $block_content);
			}
		}

		return $block_content;
	}

	/**
	 * Fix wrong quotes
	 *
	 * @param string $block_content Rendered block
	 * @param array $block Array of current block params
	 * @return string
	 */
	public function wp_fix_quotes($block_content, $block)
	{
		if ("core/code" === $block['blockName']) {
			//https://codex.wordpress.org/Writing_Code_in_Your_Posts
			$wrong_single_quotes = array(
				'/&lsquo;/i',
				'/&#8216;/i',
				'/&rsquo;/i',
				'/&#8217;/i',
				'/‘/i',
				'/’/i',
			);
			$wrong_double_quotes = array(
				'/&ldquo;/i',
				'/&#8220;/i',
				'/&rdquo;/i',
				'/&#8221;/i',
				'/”/i',
				'/“/i',
				'/&#x22;/i',
				'/&#x201D;/i',
			);
			$block_content = preg_replace($wrong_double_quotes, '&#34;', $block_content);
			$block_content = preg_replace($wrong_single_quotes, '&#39;', $block_content);
		}

		return $block_content;
	}

}
