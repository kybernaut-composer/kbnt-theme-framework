<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;

class AdminColumns extends AbstractComponent {

	/**
	 * Columns to register
	 * @var array
	 */
	private $columns = [];

	public function add_column( $meta_key, $label, $post_type, $args = [] ) {

		$defaults = [
			'sortable' => false,
			'before_column' => null,
		];

		$prepare_args = \array_merge($args,['meta_key' => $meta_key, 'label' => $label]);

		$all_args = \wp_parse_args($prepare_args, $defaults);

		$this->columns[$post_type][] = $all_args;
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{

		// Bail if no columns.
		if (empty($this->columns)) {
			return;
		}

		// Add custom columns.
		foreach ( $this->columns as $post_type => $columns) {
			foreach ($columns as $column) {

				// Add column to the right place - pass variable with closure.
				add_filter("manage_{$post_type}_posts_columns", function($existing_columns) use ( $column ) {
					return $this->wp_add_column_to_admin_archive($existing_columns, $column);
				});

				// Show post meta value.
				add_filter("manage_{$post_type}_posts_custom_column", function($current_column, $post_id) use ( $column ) {
					return $this->wp_display_column_value($current_column,$post_id, $column['meta_key']);
				}, 10, 2);

				// If sortable.
				if ( $column['sortable']) {

					// Make column sortable.
					add_filter("manage_edit-{$post_type}_sortable_columns", function($existing_columns) use ( $column ) {
						return $this->append_column_to_the_end_of_array($column, $existing_columns);
					});

					// Set how to sort.
					add_action('pre_get_posts',
					function ($query) use ($column) {
						return $this->wp_custom_order_by($query, $column['meta_key']);
					});

				}
			}
		}

	}

	/**
	 * Append column to the end of array
	 * @param array $column Column array.
	 * @param array $array Array.
	 * @return array
	 */
	public function append_column_to_the_end_of_array($column, $array) {
		$array[$column['meta_key']] = $column['label'];
		return $array;
	}

	/**
	 * Display the post meta if current column matches.
	 * @param string $current_column Current column slug.
	 * @param int $post_id Post ID.
	 * @param string $meta_key Meta key slug.
	 * @return void
	 */
	public function wp_display_column_value( $current_column, $post_id, $meta_key) {
		if ($current_column === $meta_key ){
			echo \get_post_meta( $post_id, $meta_key, true);
		}
	}

	/**
	 * Setup how to sort newly added columns
	 *
	 * @param WP_Query $query Query.
	 * @param string $meta_key Meta key name.
	 * @return void
	 */
	public function wp_custom_order_by( $query, $meta_key) {

		if (!is_admin()) {
			return;
		}

		$orderby = $query->get('orderby');

		if ($meta_key === $orderby) {
			$query->set('meta_key', $meta_key);
			$query->set('orderby', 'meta_value');
		}
	}

	/**
	 * Append column.
	 * @param array $existing_columns Existing columns.
	 * @param array $column New column array.
	 * @return mixed
	 */
	public function wp_add_column_to_admin_archive($existing_columns, $column) {

		// Append to the end.
		if ( $column['before_column'] === null ) {
			return $this->append_column_to_the_end_of_array($column, $existing_columns);
		}

		// Add before column.
		$new_columns = [];
		foreach ($existing_columns as $key => $value) {
			if ($key === $column['before_column']) {
				$new_columns[$column['meta_key']] = $column['label'];
			}
			$new_columns[$key] = $value;
		}
		return $new_columns;
	}

}
