<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;

class CPT extends AbstractComponent
{

	/**
	 * CPTS to register
	 * @var array
	 */
	private $cpts = [];

	/**
	 * Taxonomies to register
	 * @var array
	 */
	private $taxonomies = [];

	/**
	 * Archive only CPTS
	 * @var array
	 */
	private $archive_only = [];

	/**
	 * Archive show all CPTS
	 * @var array
	 */
	private $archive_display_all = [];

	/**
	 * Archive ordered by
	 * @var array
	 */
	private $archive_by = [];

	/**
	 * Taxnomy ordered by
	 * @var array
	 */
	private $tax_by = [];

	/**
	 * Remove archive title
	 * @var bool
	 */
	private $remove_archive_title = false;

	/**
	 * Set CPT with archive only page
	 * @param string $cpt
	 * @return void
	 */
	public function set_cpt_archive_only($cpt)
	{
		$this->archive_only[] = $cpt;
	}

	/**
	 * Set CPT with all posts shown on archive
	 * @param string $cpt
	 * @return void
	 */
	public function display_all_posts_in_archive($cpt)
	{
		$this->archive_display_all[] = $cpt;
	}

	/**
	 * Order by title
	 * @param string $cpt
	 * @deprecated 1.2.0 Use archive_order_by($cpt)
	 * @see archive_order_by()
	 * @return void
	 */
	public function archive_order_by_title($cpt)
	{
		_deprecated_function(__FUNCTION__, '1.2.0', 'archive_order_by($cpt)');
		$this->archive_order_by($cpt);
	}

	/**
	 * Order archive by custom parameters
	 * @param mixed $cpt Post type slug.
	 * @param string $by By parameter.
	 * @param string $how Select order of parameters.
	 * @return void
	 */
	public function archive_order_by($cpt, $by = 'title', $how = 'ASC')
	{
		$this->archive_by[] = [
			'post_type' => $cpt,
			'by' => $by,
			'how' => $how
		];
	}

	/**
	 * Order archive by custom parameters
	 * @param mixed $taxonomy Taxonomy slug.
	 * @param string $by By parameter.
	 * @param string $how Select order of parameters.
	 * @return void
	 */
	public function taxonomy_order_by($taxonomy, $by = 'title', $how = 'ASC')
	{
		$this->tax_by[] = [
			'taxonomy' => $taxonomy,
			'by' => $by,
			'how' => $how
		];
	}

	/**
	 * Register CPT
	 * @param string $slug
	 * @param string $label
	 * @param string $icon
	 * @param int $menu_position
	 * @param array $args
	 * @return void
	 */
	public function register_cpt(string $slug, string $label, string $icon = 'dashicons-lightbulb', int $menu_position = 10, array $args = [])
	{

		$fn_args = [
			'label' => $label,
			'description' => $label,
			'menu_position' => $menu_position,
			'menu_icon' => $icon,
		];

		$mergerd_args = \array_merge_recursive($fn_args, $args);

		$this->cpts[] = [
			'slug' => $slug,
			'args' => $mergerd_args,
		];
	}

	public function register_taxonomy(string $slug, string $label_singular, string $label_plural, array $post_types, array $args = [])
	{

		$fn_args = [
			'labels' => [
				'name' => $label_singular,
				'name' => $label_plural,
			]
		];

		$mergerd_args = \array_merge_recursive($fn_args, $args);

		$this->taxonomies[] = [
			'slug' => $slug,
			'post_types' => $post_types,
			'args' => $mergerd_args,
		];
	}

	/**
	 * Remove archive title
	 *
	 * Don't "Archive: Posts" do just "Posts"
	 * @return void
	 */
	public function remove_archive_title() {
		$this->remove_archive_title = true;
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{

		if ($this->cpts || $this->taxonomies) {
			add_action('init', [$this, 'wp_register_cpts_taxonomies'], 10, 1);
		}

		if ($this->archive_only) {
			add_action('template_redirect', [$this, 'wp_archive_only']);
		}

		if ($this->archive_display_all || !empty($this->archive_by )) {
			add_action('pre_get_posts', function ($query) {
				if (!is_admin() && $query->is_main_query()) {
					if ($this->archive_display_all && is_post_type_archive($this->archive_display_all)) {
						$query->set('posts_per_page', -1);
					}
					foreach( $this->archive_by as $ab) {
						if (is_post_type_archive($ab['post_type'])) {
							$query->set('orderby', [$ab['by'] => $ab['how']]);
						}
					}
					foreach( $this->tax_by as $tb) {
						if (is_tax($tb['taxonomy'])) {
							$query->set('orderby', [$tb['by'] => $tb['how']]);
						}
					}
				}
			});
		}

		if ( $this->remove_archive_title ) {
			add_filter('get_the_archive_title', function ($title) {
				if (is_category()) {
					$title = single_cat_title('', false);
				} elseif (is_tag()) {
					$title = single_tag_title('', false);
				} elseif (is_author()) {
					$title = '<span class="vcard">' . get_the_author() . '</span>';
				} elseif (is_tax()) { //for custom post types
					$title = sprintf(__('%1$s'), single_term_title('', false));
				} elseif (is_post_type_archive()) {
					$title = post_type_archive_title('', false);
				}
				return $title;
			});
		}
	}

	/**
	 * Redirect page to the archive CPT
	 * @source https://themeitems.com/how-to-create-a-archive-only-custom-post-type-in-wordpress/
	 * @return void
	 */
	public function wp_archive_only()
	{
		foreach ($this->archive_only as $cpt) {
			global $post;
			if (is_singular($cpt)) {
				$redirect_link = get_post_type_archive_link($cpt) . "#" . sanitize_title($post->post_title);
				wp_safe_redirect($redirect_link, 302);
				exit;
			}
		}
	}

	/**
	 * Register CPTs
	 * @return void
	 */
	public function wp_register_cpts_taxonomies()
	{

		if ($this->cpts) {

			$default_args_cpts = [
				'supports' => ['title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'author'],
				'hierarchical'        => true,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => true,
				'can_export'          => true,
				'has_archive'         => false,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'capability_type'     => 'page',
				'show_in_rest'        => true,
			];

			foreach ($this->cpts as $cpt) {

				$args = \wp_parse_args($cpt['args'], $default_args_cpts);

				\register_post_type($cpt['slug'], $args);
			}
		}

		if ($this->taxonomies) {

			$default_args_tax = [
				'hierarchical'      => true,
				'public'            => false,
				'show_ui'           => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => false,
				'show_tagcloud'     => false,
				'show_in_rest'      => true,
			];

			foreach ($this->taxonomies as $tax) {

				$args = \wp_parse_args($tax['args'], $default_args_tax);

				\register_taxonomy(
					$tax['slug'],
					$tax['post_types'],
					$args,
				);
			}
		}
	}
}
