<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;
use KBNT_Framework\Helpers\SVG;

class Theme extends AbstractComponent
{

	/**
	 * Text domain
	 * @var string
	 */
	private $textdomain = 'kbnt';

	/**
	 * Image sizes to add
	 * @var array
	 */
	private $image_sizes_add = [];

	/**
	 * Image sizes to remove
	 * @var array
	 */
	private $image_sizes_remove = [];

	/**
	 * Overwrite thumbnail size
	 * @var (int|bool)[]
	 */
	private $image_thumbnail = [];

	/**
	 * Overwrite medium size
	 * @var (int|bool)[]
	 */
	private $image_medium = [];

	/**
	 * Overwrite large size
	 * @var (int|bool)[]
	 */
	private $image_large = [];

	/**
	 * Register menus
	 * @var array
	 */
	private $menu_register = [];

	/**
	 * Don't load WPML scripts
	 * @var false
	 */
	private $wpml_css_dont_load = false;

	/**
	 * Remove body class
	 * @var array
	 */
	private $remove_body_classes = [];

	/**
	 * Theme support - blocks align wide
	 * @var false
	 */
	private $support_align_wide = false;

	/**
	 * Theme support - blocks custom units
	 * TODO move to gutenberg
	 * @var false
	 */
	private $support_custom_units = false;

	/**
	 * Theme support - custom font sizes in PX
	 * TODO move to gutenberg
	 * @var true
	 */
	private $disable_custom_font_sizes = true;

	/**
	 * Theme support - default core block patterns sizes in PX
	 * TODO move to gutenberg
	 * @var true
	 */
	private $disable_core_block_patterns = true;

	/**
	 * Select post types for search
	 * @var true
	 */
	private $search_in = [];

	/**
	 * Add post state to page template
	 * @var array
	 */
	private $post_states = [];

	/**
	 * Set textdomain
	 * @param string $textdomain
	 * @return void
	 */
	public function load_textdomain(string $textdomain)
	{
		$this->textdomain = $textdomain;
	}

	/**
	 * Add menu support
	 * @param string $slug Menu ID.
	 * @param string $name Menu name.
	 * @return void
	 */
	public function add_menu(string $slug, string $name)
	{
		$this->menu_register[$slug] = \esc_html($name);
	}

	/**
	 * Register new image size
	 * @param string $name Name(slug).
	 * @param int $width Width in px.
	 * @param int $height Height in px.
	 * @param bool $crop Crop in px.
	 * @return void
	 */
	public function add_image_size(string $name, int $width, $height = 0, $crop = false)
	{
		$this->image_sizes_add[] = [$name, $width, $height, $crop];
	}

	/**
	 * Modify default thumbnail size
	 * @param int $width Width in px.
	 * @param int $height Height in px.
	 * @param bool $crop Crop in px.
	 * @return void
	 */
	public function image_thumbnail(int $width, $height = 0, $crop = false)
	{
		$this->image_thumbnail = [$width, $height, $crop];
	}

	/**
	 * Modify default medium size
	 * @param int $width Width in px.
	 * @param int $height Height in px.
	 * @param bool $crop Crop in px.
	 * @return void
	 */
	public function image_medium(int $width, $height = 0, $crop = false)
	{
		$this->image_medium = [$width, $height, $crop];
	}

	/**
	 * Modify default large size
	 * @param int $width Width in px.
	 * @param int $height Height in px.
	 * @param bool $crop Crop in px.
	 * @return void
	 */
	public function image_large(int $width, $height = 0, $crop = false)
	{
		$this->image_large = [$width, $height, $crop];
	}

	/**
	 * Remove unused image size
	 * @param string $name Size name/slug.
	 * @return void
	 */
	public function remove_image_size(string $name)
	{
		$this->image_sizes_remove[] = $name;
	}

	/**
	 * Prevent WPML css styles from loading
	 * @return void
	 */
	public function wpml_dont_load_css()
	{
		$this->wpml_css_dont_load = true;
	}

	/**
	 * Pass body classes to remove
	 * @param array $classes
	 * @return void
	 */
	public function body_class_remove(array $classes)
	{
		$this->remove_body_classes = $classes;
	}

	/**
	 * Enable align wide support for Gutenberg
	 * @return void
	 */
	public function add_support_align_wide()
	{
		$this->support_align_wide = true;
	}

	/**
	 * Enable custom units support for Gutenberg
	 * @return void
	 */
	public function add_support_custom_units()
	{
		$this->support_custom_units = true;
	}

	/**
	 * Enable user manually editing pixel size support for Gutenberg
	 * @return void
	 */
	public function add_support_custom_font_sizes()
	{
		$this->disable_custom_font_sizes = false;
	}

	/**
	 * Enable core block patterns
	 * @return void
	 */
	public function add_support_core_block_patterns()
	{
		$this->disable_core_block_patterns = false;
	}

	/**
	 * Search in selected CPTS
	 * @param array $cpts
	 * @return void
	 */
	public function search_only_in(array $cpts)
	{
		$this->search_in = $cpts;
	}

	/**
	 * Add a post state to selected template
	 * @param string $page_template Page template name.
	 * @param string $state State label.
	 * @return void
	 */
	public function add_post_state($page_template, $state)
	{
		$this->post_states[] = [
			'page_template' => $page_template,
			'state' => $state,
		];
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{
		add_action('sanitize_file_name', array($this, 'wp_sanitize'));
		add_action('after_setup_theme', array($this, 'wp_theme_supports'));

		if ($this->search_in) {
			add_filter('pre_get_posts', array($this, 'wp_pre_get_posts'));
		}

		if ($this->image_thumbnail || $this->image_medium || $this->image_large) {
			add_action('after_switch_theme', array($this, 'wp_theme_activated'));
		}

		if ($this->remove_body_classes) {
			add_filter('body_class',  array($this, 'wp_body_class'), 10, 2);
		}

		if ($this->post_states) {
			add_filter('display_post_states',  array($this, 'wp_display_post_states'), 10, 2);
		}
	}

	/**
	 * Sanitize filename when uploaded to Media library
	 * @param mixed $filename Filename.
	 * @return string
	 */
	public function wp_sanitize($filename)
	{
		return preg_replace("/\s+/", "-", strtolower(remove_accents($filename)));
	}

	/**
	 * WP - Do upon theme activation
	 * @return void
	 */
	public function wp_theme_activated()
	{
		// Thumbnail - width, height, crop.
		if ($this->image_thumbnail) {
			update_option('thumbnail_size_w', $this->image_thumbnail[0]);
			update_option('thumbnail_size_h', $this->image_thumbnail[1]);
			update_option('thumbnail_crop', $this->image_thumbnail[2]);
		}

		// Medium - width, height, crop.
		if ($this->image_medium) {
			update_option('medium_size_w', $this->image_medium[0]);
			update_option('medium_size_h', $this->image_medium[1]);
			update_option('medium_crop', $this->image_medium[2]);
		}

		// Large - width, height, crop.
		if ($this->image_large) {
			update_option('large_size_w', $this->image_large[0]);
			update_option('large_size_h', $this->image_large[1]);
			update_option('large_crop', $this->image_large[2]);
		}
	}

	/**
	 * WP - Add theme support
	 * @return void
	 */
	public function wp_theme_supports()
	{

		// Add page template setup (broken in WP 5.8) - TODO: Remove when fixed in the Core.
		// https://wordpress.org/support/topic/page-templates-missing-from-page-attributes-after-5-8/
		add_theme_support('block-templates');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		// Add excerpt for pages.
		add_post_type_support('page', 'excerpt');

		/*
			* Let WordPress manage the document title.
			* By adding theme support, we declare that this theme does not use a
			* hard-coded <title> tag in the document head, and expect WordPress to
			* provide it for us.
			*/
		add_theme_support('title-tag');

		/*
			* Enable support for Post Thumbnails on posts and pages.
			*
			* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
			*/
		add_theme_support('post-thumbnails');

		/*
			* Switch default core markup for search form, comment form, and comments
			* to output valid HTML5.
			*/
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Gutenberg
		if ($this->support_align_wide) {
			add_theme_support('align-wide');
		}
		if ($this->support_custom_units) {
			add_theme_support('custom-units');
		}
		if ($this->disable_custom_font_sizes) {
			add_theme_support('disable-custom-font-sizes');
		}
		if ($this->disable_core_block_patterns) {
			\remove_theme_support('core-block-patterns');
		}

		// Load theme textdomain.
		load_theme_textdomain($this->textdomain, get_template_directory() . '/languages');

		// WPML dont load unnecessary CSS.
		if ($this->wpml_css_dont_load) {
			define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
			define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
		}

		// Register menus.
		if ($this->menu_register) {
			register_nav_menus(
				$this->menu_register
			);
			add_theme_support('menus');
		}

		// Add new image sizes.
		if ($this->image_sizes_add) {
			foreach ($this->image_sizes_add as $size) {
				add_image_size(...$size); // 'w540', 540, 0, false
			}
		}

		// Remove image sizes.
		if ($this->image_sizes_remove) {
			foreach ($this->image_sizes_remove as $size) {
				remove_image_size($size); // '2048x2048'
			}
		}
	}

	/**
	 * Modify body classes
	 * @param array $body_classes
	 * @param array $extra_classes
	 * @return mixed
	 */
	public function wp_body_class(array $body_classes, array $extra_classes)
	{

		if ($this->remove_body_classes) {

			// Filter the body classes and keep not the blacklisted ones
			$body_classes = array_diff($body_classes, $this->remove_body_classes);

			// Add the extra classes back untouched
			$body_classes = array_merge($body_classes, (array) $extra_classes);
		}

		return $body_classes;
	}

	public function allow_svg()
	{
		$svg = new SVG();
		$svg->init();
	}

	/**
	 * Fires after the query variable object is created, but before the actual query is run.
	 * @param mixed $query The WP_Query instance (passed by reference).
	 * @return void
	 */
	public function wp_pre_get_posts($query)
	{

		if ($this->search_in) {
			if ($query->is_search && !is_admin() && $query->is_main_query()) {
				$query->set('post_type', $this->search_in);
			}
		}

		return $query;
	}

	/**
	 * Adds a post state to selected page templates
	 *
	 * @param array $post_states The current array of post states.
	 * @param WP_Post $post The current post object.
	 * @return void
	 */
	public function wp_display_post_states($post_states, $post)
	{
		foreach ($this->post_states as $ps) {
			if ($post->__isset('page_template') && $post->__get('page_template') === $ps['page_template']) {
				$post_states[sanitize_title($ps['state'])] = $ps['state'];
			}
		}
		return $post_states;
	}
}
