<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;
use WP_User;

class Security extends AbstractComponent
{

	/**
	 * Text domain
	 * @var string
	 */
	private $textdomain = 'kbnt';

	/**
	 * Login errors message
	 *
	 * @var null|string
	 */
	private $_login_errors = null;

	/**
	 * Disable application password in WP >=5.6
	 * @var false
	 */
	private $_disable_application_password = false;

	/**
	 * Disable application password in WP >=5.6
	 * @var false
	 */
	private $_restrict_application_password = null;

	/**
	 * Disable author pages
	 * @var false
	 */
	private $_disable_author_pages = false;

	/**
	 * Disable user REST API endpoints
	 * @var false
	 */
	private $_disable_user_endpoints = false;

	/**
	 * Enable REST functionality only for logged in users
	 * @var false
	 */
	private $_enable_rest_only_for_logged_in_users = false;

	/**
	 * Set textdomain
	 * @param string $textdomain
	 * @return void
	 */
	public function load_textdomain(string $textdomain)
	{
		$this->textdomain = $textdomain;
	}

	/**
	 * Don't reveal valid users in login errors (brute force)
	 *
	 * @see https://www.wordfence.com/help/firewall/brute-force/
	 * @param string $string
	 * @return void
	 */
	public function custom_login_error($string = '')
	{
		$this->_login_errors = $string ? $string : __('The username or password you entered is incorrect', $this->textdomain);
	}

	/**
	 * Disable application password in WP >=5.6
	 * @see: https://metabox.io/wordpress-disable-application-passwords/
	 * @return void
	 */
	public function disable_application_password()
	{
		$this->_disable_application_password = true;
	}

	/**
	 * Allow application passwords only for user_can
	 * @param string $capability User capability which will be able to use Application passwords
	 * @see: https://metabox.io/wordpress-disable-application-passwords/
	 * @return void
	 */
	public function restrict_application_password($capability = 'manage_options')
	{
		$this->_restrict_application_password = $capability;
	}

	/**
	 * Disable author's pages and return 404
	 * @return void
	 */
	public function disable_author_pages()
	{
		$this->_disable_author_pages = true;
	}

	/**
	 * Disable user REST API endpoints
	 * @return void
	 */
	public function disable_user_endpoints()
	{
		$this->_disable_user_endpoints = true;
	}

	/**
	 * Enable REST functionality only for logged in users
	 * @return void
	 */
	public function enable_rest_only_for_logged_in_users()
	{
		$this->_enable_rest_only_for_logged_in_users = true;
	}


	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{
		// Don't reveal valid users in login errors (brute force).
		if ($this->_login_errors) {
			add_filter('login_errors', function () {
				return $this->_login_errors;
			});
		}

		// Handle Application Passwords.
		if ($this->_disable_application_password) {
			add_filter('wp_is_application_passwords_available', '__return_false');
		}
		if ($this->_restrict_application_password) {
			add_filter('wp_is_application_passwords_available_for_user', [$this, 'wp_customize_app_password_availability'], 10,	2);
		}

		// Auhor related security
		if ($this->_disable_author_pages) {
			// Redirect page to 404
			add_filter('template_redirect', [$this, 'wp_disable_author_pages'], 10);
			// Remove from sitemap
			add_filter('wp_sitemaps_add_provider', function ($provider, $name) {
				return ($name == 'users') ? false : $provider;
			}, 10, 2);
		}
		if ($this->_disable_user_endpoints) {
			add_filter('rest_endpoints', [$this, 'wp_disable_user_endpoints']);
		}
		if ($this->_enable_rest_only_for_logged_in_users) {
			add_filter('rest_authentication_errors', [$this, 'wp_enable_rest_only_for_logged_in_users']);
			remove_action('wp_head', 'rest_output_link_wp_head', 10);
		}
	}

	/**
	 * Allow application passwords only for user_can
	 * @see: https://metabox.io/wordpress-disable-application-passwords/
	 *
	 * @param bool $available Could use App passwords?
	 * @param WP_User $user Current user
	 * @return mixed
	 */
	public function wp_customize_app_password_availability($available, $user)
	{
		if (!user_can($user, $this->_restrict_application_password)) {
			$available = false;
		}
		return $available;
	}

	/**
	 * Disable author archive page and return 404
	 * @return void
	 */
	public function wp_disable_author_pages()
	{
		global $wp_query;
		if (is_author()) {
			$wp_query->set_404();
			status_header(404);
		}
	}

	/**
	 * Disable user REST API endpoints when user is not logged in.
	 * @param array $endpoints The available endpoints. An array of matching regex patterns, each mapped to an array of callbacks for the endpoint. These take the format '/path/regex' => array( $callback, $bitmask ) or `'/path/regex' => array( array( $callback, $bitmask ).
	 * @return array
	 */
	public function wp_disable_user_endpoints($endpoints)
	{
		if (! \is_user_logged_in() ) {
			if (isset($endpoints['/wp/v2/users'])) {
				unset($endpoints['/wp/v2/users']);
			}
			if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
				unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
			}
		}
		return $endpoints;
	}

	/**
	 * Allow REST only for logged in users
	 *
	 * @param WP_Error|null|true $errors WP_Error if authentication error, null if authentication method wasn't used, true if authentication succeeded.
	 * @return mixed
	 */
	public function wp_enable_rest_only_for_logged_in_users($errors)
	{
		if (!is_user_logged_in()) {
			return new \WP_Error('rest_not_logged_in', __('Only authenticated users can access the REST API.', $this->textdomain), array('status' => 401));
		}
		return $errors;
	}
}
