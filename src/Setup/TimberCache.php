<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;

class TimberCache extends AbstractComponent {

	private $hours;
	private $hash;
	private $admin_menu;

	public function __construct()
	{
		$this->hours = 10 * \HOUR_IN_SECONDS;
		$this->hash = \bin2hex(random_bytes(18));
		$this->admin_menu = false;
	}

	/**
	 * Set expiration time in hours
	 * @var KBNT_Framework\Setup\funtion
	 */
	public function set_expiration_in_hours( $hours ) {
		$this->hours = $hours * \HOUR_IN_SECONDS;
	}

	/**
	 * Set hash for rest_api
	 */
	public function set_hash( $hash ) {
		$this->hash = $hash;
	}

	/**
	 * Enable experimental admin menu
	 */
	public function enable_admin_menu() {
		$this->admin_menu = true;
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init() {

		// Bail if no Timber installed.
		if (!class_exists('Timber')) {
			return;
		}

		// Make sure, that everycaching is REALLY empty.
		if (\Timber::$cache === false) {
			$this->clear_twig_cache();
			return;
		}

		// Otherwise, implement caching.

		\add_filter('KBNT_Framework\Helpers\TimberCache_hours', $this->hours);
		\add_action('rest_api_init', [$this,  'wp_add_clearing_rest_endpoint']);

		if ($this->admin_menu) {
			\Routes::map(
				'maintenance/clear-twig-cache',
				function () {
					if (is_user_logged_in() && current_user_can('manage_options')) {
						$this->clear_twig_cache();
						// TODO něco lepšího.
						wp_redirect(admin_url());
						exit();
					}
				}
			);
		}

	}

	/**
	 * Clear timber cahce action
	 */
	public function clear_twig_cache() {
		// Clear the Twig file (no data)
		$loader = new \Timber\Loader();
		$loader->clear_cache_twig();
		// Clear the Twig file & data in DB transient
		global $wpdb;
		$query = $wpdb->prepare(
			"DELETE FROM $wpdb->options WHERE option_name LIKE '%s'",
			'_transient%timberloader_%'
		);
		$wpdb->query($query);
		return true;
	}

	/**
	 * Add Rest API Endpoint for clearing cache
	 */
	public function wp_add_clearing_rest_endpoint() {
		register_rest_route('maintenance/v1', '/clear-twig-cache/(?P<hash>\w+)', array(
			'methods' => 'GET',
			'callback' => [$this, 'clear_twig_cache'],
			'permission_callback' => '__return_true',
			'args' => array(
				'hash' => array(
					'validate_callback' => function ($param, $request, $key) {
						return $param === $this->hash ? true : false;
					}
				),
			),
		));
	}

	/**
	 * Add admin menu toolbar item
	 */
	public function wp_add_toolbar_item($admin_bar) {
		$admin_bar->add_menu(
			array(
				'id'    => 'kbnt-timber-clear-cache',
				'title' => __('Clear Timber Cache'),
				'href'  => '/wp-json/maintenance/v1/clear-twig-cache/' . $this->hash,
				'meta'  => array(
					'title' => __('Clear Timber Cache'),
				),
			)
		);
	}

	/**
	 * Set expire time for timber cache helper function, if not WP Debug true
	 * @return int|float|false
	 * @throws ExpectationFailedException
	 */
	public static function set_expire() {
		if (defined('WP_DEBUG') && true === WP_DEBUG) {
			return false;
		}
		return \apply_filters('KBNT_Framework\Helpers\TimberCache_expiration', 10);
	}
}
