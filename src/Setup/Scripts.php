<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;
use KBNT_Framework\Helpers\DisableEmojis;

class Scripts extends AbstractComponent
{

	/**
	 * Text domain
	 * @var string
	 */
	private $textdomain = 'kbnt';

	/**
	 * Scripts to register
	 * @var array
	 */
	private $scripts = [];

	/**
	 * Load default theme style.css
	 * @var true
	 */
	private $load_style_css = false;

	/**
	 * Load default style.css as parent
	 * @var true
	 */
	private $load_style_css_parent = false;

	/**
	 * Styles to register
	 * @var array
	 */
	private $styles = [];

	/**
	 * Styles to dequeue
	 * @var array
	 */
	private $dequeue_styles = [];

	/**
	 * Scripts to dequeue
	 * @var array
	 */
	private $dequeue_scripts = [];

	/**
	 * Disable emojis
	 * @var false
	 */
	private $disable_emojis = false;

	/**
	 * Disable embeds
	 * @var false
	 */
	private $disable_embeds = false;

	/**
	 * Load scripts asynchronously;
	 * @var false
	 */
	private $async_scripts = false;

	/**
	 * Exclude JS handles from async
	 * @var array
	 */
	private $async_scripts_exclude = [];

	/**
	 * Debug async scripts
	 * @var false
	 */
	private $debug_async_scripts = false;

	/**
	 * Template/Stylesheet Directory URI
	 * @var string
	 */
	private $directory_uri = '';

	/**
	 * Load jQuery migrate
	 * @var true
	 */
	private $load_jquery_migrate = true;

	/**
	 * Template/Stylesheet Directory
	 * @var string
	 */
	private $directory = '';

	public function __construct()
	{
		$this->directory = get_template_directory();
		$this->directory_uri = get_template_directory_uri();
	}

	/**
	 * Set textdomain
	 * @param string $textdomain
	 * @return void
	 */
	public function load_textdomain(string $textdomain)
	{
		$this->textdomain = $textdomain;
	}

	/**
	 * Use stylesheet directory instead of template
	 */
	public function use_stylesheet_directory()
	{
		$this->directory = get_stylesheet_directory();
		$this->directory_uri = get_stylesheet_directory_uri();
	}

	/**
	 * Register script
	 * @param string $name Name of file without extension.
	 * @param string $path Path starting with slash.
	 * @param array $deps Dependencies.
	 * @param bool $in_footer Load in footer?
	 * @param array $localize Localization variables
	 * @param ?string $inline Inline script to append
	 * @return void
	 */
	public function register_script(string $name, string $path, array $deps = [], bool $in_footer = true, $localize = [], $inline = null)
	{
		$this->scripts[] = [
			'name' => $name,
			'path' => $path,
			'deps' => $deps,
			'in_footer' => $in_footer,
			'localize' => $localize,
			'inline' => $inline
		];
	}

	/**
	 * Dequeue style
	 * @param string $handle style handle.
	 * @return void
	 */
	public function dequeue_style(string $handle)
	{
		$this->dequeue_styles[] = $handle;
	}

	/**
	 * Dequeue script
	 * @param string $handle script handle.
	 * @return void
	 */
	public function dequeue_script(string $handle)
	{
		$this->dequeue_scripts[] = $handle;
	}

	/**
	 * Disable emojis
	 * @return void
	 */
	public function disable_emojis()
	{
		$this->disable_emojis = true;
	}

	/**
	 * Disable embeds
	 * @return void
	 */
	public function disable_embeds()
	{
		$this->disable_embeds = true;
	}

	/**
	 * Enable style.css load
	 * @param bool $load_in_child_theme Load style.css in child theme.
	 * @return void
	 */
	public function enable_style_css($load_in_child_theme = false)
	{
		$this->load_style_css = true;
		$this->load_style_css_parent = $load_in_child_theme;
	}

	/**
	 * Register style
	 * @param string $name Name of file without extension.
	 * @param string $path Path starting with slash.
	 * @param bool $enqueue Enqueue or not
	 * @param array $deps Dependencies.
	 * @return void
	 */
	public function register_style(string $name, string $path, bool $enqueue = true, array $deps = [], string $version = null, $media = 'all')
	{
		$this->styles[] = [
			'name' => $name,
			'path' => $path,
			'deps' => $deps,
			'version' => $version,
			'media' => $media,
			'enqueue' => $enqueue
		];
	}

	/**
	 * Load scripts asynchronously
	 * @param array $exclude JS handles to exclude from async.
	 * @return void
	 */
	public function enable_async_scripts($exclude = [])
	{
		$this->async_scripts = true;
		$this->async_scripts_exclude = $exclude;
	}

	/**
	 * Enable debugging in async scripts
	 * @return void
	 */
	public function enable_async_scripts_debug()
	{
		$this->debug_async_scripts = true;
	}

	/**
	 * Disable jQuery migrate
	 * @return void
	 */
	public function disable_jquery_migrate()
	{
		$this->load_jquery_migrate = false;
	}

	/**
	 * Hook into WP function and do the magic
	 * @return void
	 */
	public function init()
	{

		if (!$this->load_jquery_migrate) {
			add_action('wp_default_scripts', array($this, 'wp_remove_jquery_migrate'));
		}

		add_action('wp_enqueue_scripts', array($this, 'wp_scripts_styles'), 1000, 1);

		if ($this->dequeue_styles) {
			add_action('wp_print_styles', array($this, 'wp_dequeue_styles'), 1000);
		}

		if ($this->disable_emojis) {
			new DisableEmojis();
		}

		if ($this->disable_embeds) {
			add_action('init', array($this, 'wp_disable_embeds'), 9999);
		}

		if ($this->async_scripts) {
			add_filter('script_loader_tag', array($this, 'wp_async_attr'), 10, 3);
		}
	}

	/**
	 * Dequeue styles
	 * @return void
	 */
	public function wp_dequeue_styles()
	{
		// Dequeue styles
		foreach ($this->dequeue_styles as $ds) {
			\wp_dequeue_style($ds);
		}
	}

	/**
	 * Load scripts and styles
	 * @return void
	 */
	public function wp_scripts_styles()
	{

		// Remove scripts
		if ($this->dequeue_scripts) {
			foreach ($this->dequeue_scripts as $ds) {
				\wp_deregister_script($ds);
				\wp_dequeue_script($ds);
			}
		}

		// Setup script debug .min version.
		$suffix = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min');

		// Load style.css
		if ($this->load_style_css) {
			if (is_child_theme()) {
				if ($this->load_style_css_parent) {
					wp_enqueue_style($this->textdomain, get_template_directory_uri() . '/style.css', array(), filemtime(get_template_directory() . '/style.css'));
				}
			} else {
				wp_enqueue_style($this->textdomain, get_stylesheet_uri(), array(), filemtime(get_stylesheet_directory() . '/style.css'));
			}
		}

		foreach ($this->styles as $style) {
			// register all
			if ($this->is_external_link($style['path'])) {
				\wp_register_style(
					$this->textdomain . '-' . $style['name'],
					$style['path'],
					$style['deps'],
					$style['version'],
					$style['media']
				);
			} else {
				$name_with_path = trailingslashit($style['path']) . $style['name'] . '.css';
				\wp_register_style(
					$this->textdomain . '-' . $style['name'],
					$this->directory_uri . $name_with_path,
					$style['deps'],
					filemtime($this->directory . $name_with_path),
					$style['media']
				);
			}

			// enqueue some
			if ($style['enqueue']) {
				wp_enqueue_style($this->textdomain . '-' . $style['name']);
			}
		}

		// Enqueue scripts
		foreach ($this->scripts as $script) {
			if ($this->is_external_link($script['path'])) {
				wp_enqueue_script(
					$this->textdomain . '-' . $script['name'],
					$script['path'],
					$script['deps'],
					false,
					$script['in_footer']
				);
			} else {

				$name_with_path =  trailingslashit($script['path']) . $script['name'];
				// Test if minified file has suffix, if not try to load directly.
				if ( \file_exists($this->directory . $name_with_path . $suffix . '.js') ) {
					$this->enqueue_script_helper($name_with_path . $suffix . '.js', $script);
				} elseif ($suffix === '.min' && \file_exists($this->directory . $name_with_path . '.js')) {
					$this->enqueue_script_helper($name_with_path . '.js', $script);
				}
			}
			if (!empty($script['localize'])) {
				wp_localize_script($this->textdomain . '-' . $script['name'], 'props', $script['localize']);
			}
			if ($script['inline']) {
				wp_add_inline_script(
					$this->textdomain . '-' . $script['name'],
					$script['inline']
				);
			}
		}
	}

	/**
	 * Helper for loading scripts
	 * @param string $name_with_path script name and path.
	 * @param array $script Array with script settings.
	 * @return void
	 */
	private function enqueue_script_helper($name_with_path, $script) {
		wp_enqueue_script(
			$this->textdomain . '-' . $script['name'],
			$this->directory_uri . $name_with_path,
			$script['deps'],
			filemtime($this->directory . $name_with_path),
			$script['in_footer']
		);
	}

	/**
	 * Remove jQuery migrate when not used on frontend
	 * @param mixed $scripts Scripts.
	 * @return void
	 */
	public function wp_remove_jquery_migrate($scripts)
	{
		if (!is_admin() && isset($scripts->registered['jquery'])) {
			$script = $scripts->registered['jquery'];

			if ($script->deps) { // Check whether the script has any dependencies
				$script->deps = array_diff($script->deps, array(
					'jquery-migrate'
				));
			}
		}
	}

	/**
	 * Remove WP Embeds when not used on frontend
	 *
	 * @see https://kinsta.com/knowledgebase/disable-embeds-wordpress/
	 * @param mixed $scripts Scripts.
	 * @return void
	 */
	public function wp_disable_embeds()
	{
		// Remove the REST API endpoint.
		remove_action('rest_api_init', 'wp_oembed_register_route');

		// Turn off oEmbed auto discovery.
		add_filter('embed_oembed_discover', '__return_false');

		// Don't filter oEmbed results.
		remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

		// Remove oEmbed discovery links.
		remove_action('wp_head', 'wp_oembed_add_discovery_links');

		// Remove oEmbed-specific JavaScript from the front-end and back-end.
		remove_action('wp_head', 'wp_oembed_add_host_js');
		add_filter('tiny_mce_plugins', function ($plugins) {
			return array_diff($plugins, array('wpembed'));
		});

		// Remove all embeds rewrite rules.
		add_filter('rewrite_rules_array', function ($rules) {
			foreach ($rules as $rule => $rewrite) {
				if (false !== strpos($rewrite, 'embed=true')) {
					unset($rules[$rule]);
				}
			}
			return $rules;
		});

		// Remove filter of the oEmbed result before any HTTP requests are made.
		remove_filter('pre_oembed_result', 'wp_filter_pre_oembed_result', 10);
	}

	/**
	 * Check if the URL is external or not
	 * @param string $url
	 * @return bool
	 */
	private function is_external_link($url)
	{
		if ('http' === substr($url, 0, 4) || '://' === substr($url, 0, 3)) {
			return true;
		}
		return false;
	}

	/**
	 * Add async parameter to the script
	 * @param string $tag HTML script tag.
	 * @param string $handle Script handle.
	 * @param string $src Script URL.
	 * @return string HTML script tag.
	 */
	public function wp_async_attr($tag, $handle, $src)
	{
		global $wp_scripts;

		$debug = '';
		$dependency = false;
		if ($this->debug_async_scripts) {
			$debug = '<!-- JS Handle: ' . $handle . '-->';
		}

		foreach ($wp_scripts->registered as $script) {
			if (in_array($handle, $script->deps)) {
				$dependency = true;
				break;
			}
		}

		if (!$dependency && !\in_array($handle, $this->async_scripts_exclude, true)) {
			return $debug . str_replace('src=', 'async src=', $tag);
		}
		return $debug . $tag;
	}
}
