<?php

namespace KBNT_Framework\Setup;

class Main
{

	/**
	 * Text domain
	 * @var string
	 */
	private $textdomain;

	public $cpt;
	public $scripts;
	public $theme;
	public $gutenberg;
	public $tracking;
	public $security;
	public $timbercache;

	function __construct($textdomain = 'kbnt')
	{
		$this->textdomain = $textdomain;
		$this->cpt = new CPT();
		$this->admin_columns = new AdminColumns();
		$this->theme = $this->theme();
		$this->gutenberg = new Gutenberg();
		$this->tracking = new Tracking();
		$this->security = $this->security();
		$this->timbercache = new TimberCache();
	}

	/**
	 * Load Theme related functions
	 * @return Theme
	 */
	private function theme()
	{
		$theme = new Theme();
		$theme->load_textdomain($this->textdomain);
		return $theme;
	}

	/**
	 * Load Security related functions
	 * @return Security
	 */
	private function security()
	{
		$security = new Security();
		$security->load_textdomain($this->textdomain);
		return $security;
	}
}
