<?php

namespace KBNT_Framework\Setup;

use KBNT_Framework\Abstracts\AbstractComponent;

class Tracking extends AbstractComponent
{

	private $ga = false;
	private $gtm = false;

	/**
	 * Set Google Analytics ID
	 * @param string $id GA ID.
	 * @return void
	 */
	public function ga(string $id)
	{
		$this->ga = $id;
	}

	/**
	 * Set GTM ID
	 * @param string $id GTM ID.
	 * @return void
	 */
	public function gtm(string $id)
	{
		$this->gtm = $id;
	}

	/**
	 * Hook into WordPress
	 * @return void
	 */
	public function init()
	{
		add_action('wp_head', array($this, 'wp_code_head'));
		add_action('wp_body_open', array($this, 'wp_code_body'));
	}

	/**
	 * Insert code into head
	 * @return void
	 */
	public function wp_code_head()
	{
		if ($this->gtm) {
			echo "<!-- Google Tag Manager -->
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','" . $this->gtm . "');</script>
				<!-- End Google Tag Manager -->";
		}
		if ($this->ga) {
			echo "<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src='https://www.googletagmanager.com/gtag/js?id=<?php echo $this->id; ?>'></script>
			<script>window.dataLayer = window.dataLayer || [];

				function gtag() {
					dataLayer.push(arguments);
				}

				gtag('js', new Date());
				gtag('config', '" . $this->ga . "');</script>";
		}
	}

	/**
	 * Insert code after body tag
	 * @return void
	 */
	public function wp_code_body()
	{
		if ($this->gtm) {
			echo "<!-- Google Tag Manager (noscript) -->
				<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=" . $this->gtm . "' height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
				<!-- End Google Tag Manager (noscript) -->";
		}
	}
}
