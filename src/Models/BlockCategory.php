<?php

namespace KBNT_Framework\Models;

class BlockCategory
{

	public ?string $slug = null;
	public string $title;
	public ?string $icon = null;

	function __construct(string $title)
	{
		$this->title = $title;
	}

	/**
	 * Get block category data
	 * @return array []
	 */
	function get()
	{
		if ($this->slug === null) {
			$this->slug = \sanitize_title($this->title);
		}

		return [
			'slug' => $this->slug,
			'title' => $this->title,
			'icon' => $this->icon,
		];
	}
}
