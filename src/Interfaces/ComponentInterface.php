<?php

namespace KBNT_Framework\Interfaces;

interface ComponentInterface
{
	/**
	 * Init function
	 * @return bool
	 */
	public function init();
}
