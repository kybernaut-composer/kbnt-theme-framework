<?php

namespace KBNT_Framework\Abstracts;

use KBNT_Framework\Interfaces\ComponentInterface;

abstract class AbstractComponent implements ComponentInterface
{
}
