<?php

/**
 * CF 7 Heplers
 *
 * @package    WordPress
 * @subpackage KBNT_Framework
 * @since 0.3.0
 * @author Karolina Vyskocilova <karolina@kybernaut.cz>
 */
namespace KBNT_Framework\Helpers;

/**
 * Bunch of CF7 performance related helpers
 * @package KBNT_Framework\Helpers
 */
class CF7
{

	/**
	 * Enable lazyloa of CF 7
	 * @var false
	 */
	private $_lazyload_form = false;

	/**
	 * Do not load style
	 * @var true
	 */
	private $_disable_css = false;

	/**
	 * Do not load script
	 * @var true
	 */
	private $_disable_js = false;

	/**
	 * Disable WP autop
	 * @var true
	 */
	private $_disable_autop = false;

	/**
	 * Load script only when CF7 used
	 * @var true
	 */
	private $_only_when_used = false;

	/**
	 * Load CSS only when CF7 used
	 * @var true
	 */
	private $_only_when_used_css = true;

	/**
	 * Add condition when to custom load form
	 * @var false
	 */
	private $_condition_load = false;

	/**
	 * Defer loading of ACF to form input click
	 * @param bool $condition_load Allow to pass a custom conditional check.
	 * @return void
	 */
	public function lazyload_form($condition_load = false)
	{
		$this->_lazyload_form = true;
		$this->_disable_js = true;
		$this->_condition_load = $condition_load;
	}

	/**
	 * Disable CSS Loading
	 * @return void
	 */
	public function disable_css()
	{
		$this->_disable_css = true;
	}

	/**
	 * Disable JS Loading
	 * @return void
	 */
	public function disable_js()
	{
		$this->_disable_js = true;
	}

	/**
	 * Disable <br> and <p> tags in shortcode
	 * @return void
	 */
	public function disable_autop()
	{
		$this->_disable_autop = true;
	}

	/**
	 * Load CF7 when used only
	 * @return void
	 */
	public function load_cf7_when_used($condition_load = false, $load_css = true)
	{
		$this->_disable_css = true;
		$this->_disable_js = true;
		$this->_only_when_used = true;
		$this->_only_when_used_css = $load_css;
		$this->_condition_load = $condition_load;
	}

	/**
	 * Initialize the class
	 * @return void
	 */
	public function init()
	{

		// Don't do anything if no CF7 loaded
		if (!defined('WPCF7_VERSION')) {
			return;
		}

		// Load recaptcha only when needed per default.
		add_action('wp_enqueue_scripts', [$this, 'wp_load_recaptcha_only_when_needed'], 1000);

		if ($this->_disable_autop) {
			add_filter('wpcf7_autop_or_not', '__return_false');
		}

		if ($this->_disable_css) {
			add_filter('wpcf7_load_css', '__return_false');
		}

		if ($this->_disable_js) {
			add_filter('wpcf7_load_js', '__return_false');
		}

		if ($this->_only_when_used) {
			add_action('wp_enqueue_scripts', [$this, 'wp_load_scripts_styles'], 1000);
		}

		if ($this->_lazyload_form) {
			add_action('wp_footer', [$this, 'wp_lazyinit_js']);
		}
	}

	/**
	 * Handle lazyloading CF7 and jQuery if needed as well :)
	 * @return void
	 */
	public function wp_lazyinit_js()
	{

		// Bail if no CF7 used
		if (!$this->has_cf7()) {
			return;
		}

		// localize script
		// Source contact-form-7\includes\controller.php, line 42-55
		$wpcf7 = [
			'apiSettings' => [
				'root' => esc_url_raw(rest_url('contact-form-7/v1')),
				'namespace' => 'contact-form-7/v1',
			]
		];
		if (defined('WP_CACHE') && WP_CACHE) {
			$wpcf7['cached'] = 1;
		}
		if (\wpcf7_support_html5_fallback()) {
			$wpcf7['jqueryUi'] = 1;
		}

		// GET CF7 URL
		if (\version_compare(\WPCF7_VERSION, '5.3.2', '<=')) {
			$cf7_js_url = \plugins_url('contact-form-7/includes/js/scripts.js?ver=' . \WPCF7_VERSION);
		} else {
			$cf7_js_url = \plugins_url('contact-form-7/includes/js/index.js?ver=' . \WPCF7_VERSION);
		}

		// Handle jquery-core
		$suffix     = wp_scripts_get_suffix();
		$jquery_url = "/wp-includes/js/jquery/jquery$suffix.js?ver=3.5.1";

		echo '<script>';
		echo 'var wpcf7 = ' . \json_encode($wpcf7) . ';'; // Localize
		echo ';(function() {'; // Scope start
		echo "var cf7_controls = document.querySelectorAll('.wpcf7-form-control');var cf7_lazyloaded = false;";
		echo "cf7_controls.forEach(() => this.addEventListener('click', lazyload_cf7_script));";
		echo "function lazyload_cf7_script(){if (! window.jQuery){get_script('" . $jquery_url . "')};setTimeout( function() {if (! cf7_lazyloaded){get_script('" . $cf7_js_url . "');cf7_lazyloaded=true;}}, 500)}";
		echo "get_script=(url)=>{
	var script = document.createElement('script');
	script.setAttribute('type', 'text/javascript');
	script.setAttribute('src', url);
	document.body.appendChild(script);
 }";
		echo '})()'; // Scope end
		echo '</script>';
	}

	/**
	 * Load scripts and styles only when shorcode used
	 * @return void
	 */
	public function wp_load_scripts_styles()
	{
		if (!$this->has_cf7()) {
			return;
		}

		if (function_exists('wpcf7_enqueue_styles') && function_exists('wpcf7_enqueue_scripts')) {
			if ($this->_only_when_used_css) {
				\wpcf7_enqueue_styles();
			}
			\wpcf7_enqueue_scripts();
		}
	}

	/**
	 * Check if page/post has CF7 form
	 * @return bool
	 */
	public function has_cf7()
	{
		global $post;
		if (\has_block('contact-form-7/contact-form-selector', $post) || (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'contact-form-7')) || $this->_condition_load ) {
			return true;
		}
		return false;
	}

	/**
	 * Load recaptcha only when needed per default.
	 * @return void
	 */
	public function wp_load_recaptcha_only_when_needed()
	{
		$service = \WPCF7_RECAPTCHA::get_instance();

		if (!$service->is_active()) {
			return;
		}

		if (!$this->has_cf7()) {
			wp_deregister_script('wpcf7-recaptcha');
			wp_dequeue_script('wpcf7-recaptcha');
			wp_deregister_script('google-recaptcha');
			wp_dequeue_script('google-recaptcha');
		}
	}
}
