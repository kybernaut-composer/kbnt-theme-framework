<?php

namespace KBNT_Framework\Helpers;

class General
{

	/**
	 * Encode URL for sharing
	 * @param string $url URL to encode;
	 * @return string
	 */
	public static function url_encode($url)
	{
		return urlencode(html_entity_decode($url, ENT_COMPAT, 'UTF-8'));
	}

	/**
	 * Escape phone number for href
	 *
	 * @param string $number Telephone number.
	 * @return string
	 */
	public static function phone_href($number)
	{
		return preg_replace('/[^+0-9]/', '', $number);
	}

	/**
	 * Get first page with chosen template
	 * @param string $template The template name
	 * @return null|WP_Page URL.
	 */
	public static function get_page_with_template($template)
	{
		$pages = get_pages([
			'meta_key' => '_wp_page_template',
			'meta_value' => $template
		]);
		if (isset($pages[0])) {
			return $pages[0];
		}
		return null;
	}

	/**
	 * Get link to first page with selected template
	 * @param string $template The template name
	 * @return string|null
	 */
	public static function get_page_with_template_link($template)
	{
		$page = General::get_page_with_template($template);
		if ($page) {
			return \get_permalink($page->ID);
		}
		return null;
	}

	/**
	 * Check if current page is child page
	 * @return bool
	 */
	public static function is_child_page() {
		$post = get_post();
		if (! empty($post) && $post->post_type === 'page' && $post->post_parent) {
			return true;
		}
		return false;
	}

	/**
	 * Get youtube video ID
	 *
	 * @source https://stackoverflow.com/questions/3392993/php-regex-to-get-youtube-video-id
	 * @param string $link
	 * @return string[]|null
	 */
	public static function get_youtube_id($link)
	{
		preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+|(?<=youtube.com/embed/)[^&\n]+#", $link, $matches);
		if (isset($matches[0])) {
			return $matches[0];
		}

		return false;
	}
}
