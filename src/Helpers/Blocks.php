<?php

namespace KBNT_Framework\Helpers;

class Blocks
{
	/**
	 * Parsed blocks
	 * @var null|false|array
	 */
	private $blocks;

	/**
	 * Retrieve chosen post's data from the post content block
	 *
	 * @param string $post_content
	 * @param string $block_name
	 * @deprecated 1.2.3 Use Block::get_data_from_post_content
	 */
	public static function get_block_data($post_content, $block_name)
	{

		\_deprecated_function('get_block_data', '1.2.3', 'Block::get_data_from_post_content');
		return Block::get_data_from_post_content($post_content, $block_name);
	}

	/**
	 * Check if h1 is present in the post content
	 *
	 * Doesn't work (yet with reusable data)
	 *
	 * @param string $block
	 */
	public static function has_h1($post_content)
	{
		$blocks = \parse_blocks($post_content);

		if ($blocks) {
			foreach ($blocks as $block) {
				if (isset($block['innerBlocks'])) {
					foreach ($block['innerBlocks'] as $innerBlock) {
						if (Block::has_heading_level($innerBlock)) {
							return true;
						}
					}
				} elseif (Block::has_heading_level($block)) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Return all registered blocks by namespace aka all core blocks etc.
	 * @param string $namespace
	 * @deprecated 1.2.3 Use get_by_namespace() instead.
	 * @return array
	 *
	 */
	public static function get_blocks_by_namespace($namespace)
	{
		\_deprecated_function('get_blocks_by_namespace', '1.2.3', 'get_by_namespace');
		return self::get_by_namespace($namespace);
	}

	/**
	 * Return all registered blocks by namespace aka all core blocks etc.
	 * @param strung $namespace
	 * @return array
	 *
	 * @see https://github.com/WordPress/gutenberg/issues/12219
	 */
	public static function get_by_namespace($namespace)
	{
		$all_blocks = \WP_Block_Type_Registry::get_instance()->get_all_registered();
		$allowed_from_namespace = [];
		foreach ($all_blocks as $slug => $value) {
			if (substr($slug, 0, \strlen($namespace . '/')) === $namespace . '/') {
				$allowed_from_namespace[] = $slug;
			}
		}
		return $allowed_from_namespace;
	}

	/**
	 * Return all blocks but blacklisted
	 * @param array $blacklisted List of block names to blacklist (aka core/paragraph)
	 * @return array
	 */
	public static function get_all_but_blacklisted($blacklisted) {
		$all_blocks = \WP_Block_Type_Registry::get_instance()->get_all_registered();
		$filtered = [];
		foreach ($all_blocks as $slug => $value) {
			if (!\in_array($slug, $blacklisted, true)) {
				$filtered[] = $slug;
			}
		}
		return $filtered;
	}

	/**
	 * Has_block() implementation counting with reusable blocks
	 *
	 * @see https://github.com/WordPress/gutenberg/issues/18272
	 * @param string $block_name Block name.
	 * @return bool
	 */
	public function has_block($block_name)
	{

		if (\has_block($block_name)) {
			return true;
		}

		$this->parse_reusable_blocks();

		if (empty($this->blocks)) {
			return false;
		}

		return $this->recursive_search_within_innerblocks($this->blocks, $block_name);

	}

	/**
	 * Search for a reusable block inside innerblocks
	 *
	 * @param array $blocks Blocks to loop through.
	 * @param string $block_name BFull Block type to look for.
	 * @return true|void
	 */
	private function recursive_search_within_innerblocks($blocks, $block_name)
	{
		foreach ($blocks as $block) {
			if (isset($block['innerBlocks']) && !empty($block['innerBlocks'])) {
				$this->recursive_search_within_innerblocks($block['innerBlocks'], $block_name);
			} elseif ($block['blockName'] === 'core/block' && !empty($block['attrs']['ref']) && \has_block($block_name, $block['attrs']['ref'])) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Parse blocks if at leat one reusable block is presnt.
	 *
	 * @return void
	 */
	private function parse_reusable_blocks()
	{

		if ($this->blocks !== null) {
			return;
		}

		if (\has_block('core/block')) {
			$content = \get_post_field('post_content');
			$this->blocks = \parse_blocks($content);
			return;
		}

		$this->blocks = false;
	}
}
