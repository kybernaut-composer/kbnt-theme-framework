<?php

use KBNT_Framework\Helpers\General;
use KBNT_Framework\Tests\TestCase;

class GeneralTest extends TestCase
{

	public function testUrlEncode()
	{

		$url = 'https://kybernaut.cz/';
		$this->assertSame( General::url_encode($url), 'https%3A%2F%2Fkybernaut.cz%2F');

	}

	public function testPhoneHref()
	{

		$this->assertSame( General::phone_href('737878726'), '737878726');
		$this->assertSame( General::phone_href('+420 737 87 87 26'), '+420737878726');
		$this->assertSame( General::phone_href('+420-737878726'), '+420737878726');
		$this->assertSame( General::phone_href('+420-EMA878726'), '+420878726');

	}

	public function testGetExistingPageWithTemplateLink() {

		$post = new \stdClass;
		$post->ID = 42;

		\WP_Mock::userFunction('get_pages', [
			'args' => [
				[
					'meta_key' => '_wp_page_template',
					'meta_value' => 'template-with-existing-pages.php'
				]
			],
			'return' => [$post],
		]);

		\WP_Mock::userFunction('get_permalink',[
			'args' => 42,
			'return' => 'https://kybernaut.cz/?p=42'
		]);

		$this->assertSame(General::get_page_with_template_link('template-with-existing-pages.php'), 'https://kybernaut.cz/?p=42');

	}

	public function testGetNonexitentPageWithTemplateLink()
	{

		\WP_Mock::userFunction('get_pages', [
			'args' => [
				[
					'meta_key' => '_wp_page_template',
					'meta_value' => 'template-not-used.php'
				]
			],
			'return' => [],
		]);

		$this->assertNull(General::get_page_with_template_link('template-not-used.php'));
	}

	public function testIsChildPage()
	{

		$post = new \stdClass;

		// No post.
		\WP_Mock::userFunction('get_post', [
			'times' => 1,
			'return' => null,
		]);
		$this->assertFalse(General::is_child_page());

		// No parent set up.
		$post->post_type = 'page';
		$post->post_parent = null;
		\WP_Mock::userFunction('get_post', [
			'times' => 1,
			'return' => $post,
		]);
		$this->assertFalse(General::is_child_page());

		// Test parent page.
		$post->post_parent = 123;
		\WP_Mock::userFunction('get_post', [
			'times' => 1,
			'return' => $post,
		]);
		$this->assertTrue(General::is_child_page());

		// Test parent of different CPT.
		$post = new \stdClass;
		$post->post_type = 'webinar';
		\WP_Mock::userFunction('get_post', [
			'times' => 1,
			'return' => $post,
		]);
		$this->assertFalse(General::is_child_page());

	}
}
