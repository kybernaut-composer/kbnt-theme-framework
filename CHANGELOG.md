# Changelog

## Versions

### 2.2.2 (2024-03-25)

* Update composer enshrined/svg-sanitize to 0.16.0
* Update SVG function

### 2.2.1 (2022-11-18)

* Update composer enshrined/svg-sanitize to 0.15.4

### 2.2.0

* Fix missing theme support in WP 5.8
* Add "author" as default field in CPT registration
* When loading scripts, check non minified version as well.
* Blocks - get all but blacklist blacklisting blocks

### 2.1.2 (2021-07-29)

* Fix permission callback for Timber cache
* Empty Timber cache if not enabled but the helper is active (ie - dev site in the most of the cases)

### 2.1.1 (2021-07-22)

* Load Timber helper under main setup class
* Fix Timber cache helper rest api endpoint

### 2.1.0 (2021-07-22)

* Add Timber cache helper - cleaning REST endpoint

### 2.0.4 (2021-06-29)

* Move get YT ID helper to General.
* Get ID from embed url as well.

### 2.0.3 (2021-05-24)

* Fix: Lazy loading with CF7 > 5.3.2
* Allow excluding from async.

### 2.0.2 (2021-05-19)

* Fix: wrong filter name

### 2.0.1 (2021-04-28)

* Fix: Trying to get property 'post_type' of non-object
* Gutenberg: allow to strip of empty paragraphs from content

### 2.0.0 (2021-04-14)

* Fix namespace. KBNT_Framework!

### 1.3.3 (2021-04-14)

* New: General::is_parent_page().
* New: Add AdminColumn setup helper.
* Added SonarCloud analysis.
* Code smell fixes.
* Add custom block categories to the end of array.
* Simplify has_block() helper.
* Simplify Helpers\General::phone_href().
* PHPUnit tests for Helpers\General.

### 1.3.2 (2021-04-01)

  * Add recursive search in innerblocks.
  * Fix typo in a condition.

### 1.3.1 (2021-03-30)

  * Fix: add missing return statement.
  * Improve: search in innerBlocks.

### 1.3.0 (2021-03-30)

  * When loading style.css and the theme is a parent theme, load parent theme's style instead.
  * Disable User's REST API only on frontend.¨
  * Custom Blocks\has_block implementation checking reusable blocks.

### 1.2.6

  * Search for h1 in innerBlocks as well.

### 1.2.5 (2021-03-23)

  * Check for block heading level
  * Check if post_content contains h1 block heading

### 1.2.4 (2021-03-15)

  * Spearate "Blocks" and "Block" functionality
  * Check for block class
  * Deprecate old functions

### 1.2.3 (2021-03-15)

  * Check for block class

### 1.2.2 (2021-03-11)

  * Remove "Archive:" from archive title

### 1.2.1 (2021-03-11)

  * Add function for taxonomy ordering

### 1.2.0 (2021-03-10)

* 👌 IMPROVE:
  * Allow to disable css when loading only on CF7 pages
  * Load CF7 recaptcha only on pages when needed
  * Add function for archive ordering
  * Deprecate `CPT\archive_order_by_title()` use `CPT\archive_order_by()` instead

### 1.1.0 (2021-02-22)

* 👌 IMPROVE:
  * `CF7::has_cf7()` as public
  * CF7 - has_cf7 check for blocks
  * Localize scripts
  * Conditionally use `get_stylesheet_directory()` and `get_stylesheet_directory_uri()` instead of template ones.
  * Allow to unhook jQuery migrate

### 1.0.2 (2021-02-16)

* 👌 IMPROVE:

  * Remove jQuery migrate remove function

* 🐛 FIX:

  * Helper\ACF Compatibility with new WPML
  * Too few arguments error in block categories filter
  * BlockCategory model default values

### 1.0.1 (2021-02-11)

* 🐛 FIX:

  * Incorrect init of class SVG in the Main helper
  * Apply quote fix just on Core/Code block

* 👌 IMPROVE:

  * Gutenberg block category slug from the title if not set
  * Disable all Gutenberg color settings

### 1.0.0 (2021-02-11)

* ‼️ BREAKING:

  * move Setup\SVG class to Helpers\SVG
  * move Setup\Disable_Emojis class to Helpers\DisableEmojis

* 👌 IMPROVE:

  * use ComponentInterface and AbstractComponent for Setup components

* 📦 NEW:

  * Add Gutenberg block categories

### 0.4.3 (2021-02-10)

* 🐛 FIX: CPT loading priority

### 0.4.2 (2021-02-09)

* 🐛 FIX: Typos

### 0.4.1 (2021-02-09)

* 🐛 FIX:

  * Typo in Security constructor in Setup\Main call
  * Make function call in Setup\Security public

### 0.4.0 (2021-02-09)

* 📦 NEW:

  * Setup\Security
    * Handle Application Passwords for REST API
    * Don't reveal valid users in login errors
    * Disable author pages and remove from sitemap
    * Disable user REST API endpoints
    * Allow REST only for logged in users
  * Setup\Gutenberg - Add reusable blocks to the admin menu

* 🐛 FIX:

  * ACF/get_global_options() - use `maybe_unserialize()` instead of `unserialize`

* 👌 IMPROVE

  * Refactor Setup\Main class

### 0.3.0 (2021-01-22)

* 📦 NEW:
  * CF7 Helper

### 0.2.9 (2020-01-03)

* 🐛 FIX
  * Autoload
  * typo in tracking
  * Remove useless require
* 👌 IMPROVE
  * Remove scripts from Main
  * Gutenberg lazyload youtube video completely

### 0.2.8 (2020-01-02)

* 🐛 FIX:
  * Autoload path
  * SVG height handling
  * Work with ACF galleries
  * Remove require_once of autoloaded class
* 📦 NEW:
  * Attach post style to a template
  * Get a page with the selected template
  * Get al registered blocks by namespace
  * Order archive by title
  * Display all posts on an archive
  * Gutenberg disable full-screen directory search by default
  * CPT archive only
  * Add phone URL to general fnc
* 👌 IMPROVE:
  * Refactor SVG + load size from viewBox
  * Script enable register only
  * Archive only page - link by the name
  * Helper functions static
  * Add media to styles
  * Don't load default theme style.css
* 📖 DOC:
  * Remove outdated sources
  * Add installation instructions
