# KBNT Framework

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kybernaut-composer_kbnt-theme-framework&metric=alert_status)](https://sonarcloud.io/dashboard?id=kybernaut-composer_kbnt-theme-framework)

Tailor-made WordPress framework for themes made by Karolina Vyskočilová. Available via public repo.

## Uses

* [WP Mock](https://github.com/10up/wp_mock)

## Notes

* Plugin for drag & drop post type, taxonomy etc. ordering - [Simple Custom Post Order](https://cs.wordpress.org/plugins/simple-custom-post-order/)
